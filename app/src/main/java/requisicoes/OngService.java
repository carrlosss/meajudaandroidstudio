package requisicoes;

import java.util.List;

import classes.ONG;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface OngService {

    @GET("ongs")
    Call<List<ONG>> buscarTodasOngs();

    @GET("ongs/{cnpj}")
    Call<ONG> buscarOng(@Path("cnpj") String cnpj);

    @FormUrlEncoded
    @Headers("content-type:application/x-www-form-urlencoded")
    @POST("ongs/inserir")
    Call<Object> inserirOng(@Field("cnpj") String cnpj, @Field("senha") String senha, @Field("nome") String nomeong,
                @Field("nomeresponsavel") String nomeresponsavel, @Field("razaosocial") String razaosocial, @Field("datafundacao") String datafundacao,
                @Field("endereco") String endereco, @Field("cidade") String cidade, @Field("estado") String estado, @Field("telefone") String telefone,
                @Field("email") String email);

    @FormUrlEncoded
    @Headers("content-type:application/x-www-form-urlencoded")
    @PUT("ongs/atualizar")
    Call<Object> alterarOng(@Field("cnpj") String cnpj, @Field("senha") String senha, @Field("nomeong") String nomeong,
                                         @Field("nomeresponsavel") String nomeresponsavel, @Field("razaosocial") String razaosocial, @Field("datafundacao") String datafundacao,
                                         @Field("endereco") String endereco, @Field("cidade") String cidade, @Field("estado") String estado, @Field("telefone") String telefone,
                                         @Field("email") String email);
}
