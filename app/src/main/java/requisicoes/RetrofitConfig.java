package requisicoes;

import classes.Evento;
import classes.Voluntario;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitConfig {

    private final Retrofit retrofit;

    public RetrofitConfig() {

        this.retrofit = new Retrofit.Builder()
                .baseUrl("http://meajuda-net.umbler.net/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

    public OngService getOngService() {
        return this.retrofit.create(OngService.class);
    }

    public VoluntarioService getVoluntarioService() {
        return this.retrofit.create(VoluntarioService.class);
    }

    public EventoService getEventoService() {
        return this.retrofit.create(EventoService.class);
    }
}
