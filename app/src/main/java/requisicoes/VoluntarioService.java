package requisicoes;

import java.util.List;

import classes.ONG;
import classes.Voluntario;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface VoluntarioService {

    @GET("voluntarios/{cpf}")
    Call<Voluntario> buscarVoluntario(@Path("cpf") String cpf);

    @GET("voluntarios/voluntarioevento/{idevento}")
    Call<List<Voluntario>> buscarTodosVoluntariosEvento(@Path("idevento") int idevento);

    @FormUrlEncoded
    @Headers("content-type:application/x-www-form-urlencoded")
    @PUT("voluntarios/atualizar")
    Call<Object> alterarVoluntario(@Field("cpf") String cpf, @Field("senha") String senha, @Field("nome") String nome, @Field("datanascimento") String datanascimento,
                                   @Field("profissao") String profissao, @Field("telefone") String telefone, @Field("email") String email, @Field("cidade") String cidade,
                                   @Field("estado") String estado, @Field("endereco") String endereco);

    @FormUrlEncoded
    @Headers("content-type:application/x-www-form-urlencoded")
    @POST("voluntarios/inserir")
    Call<Object> inserirVoluntario(@Field("cpf") String cpf, @Field("senha") String senha, @Field("nome") String nome, @Field("datanascimento") String datanascimento,
                                   @Field("profissao") String profissao, @Field("telefone") String telefone, @Field("email") String email, @Field("cidade") String cidade,
                                   @Field("estado") String estado, @Field("endereco") String endereco);





}
