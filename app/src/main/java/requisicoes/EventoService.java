package requisicoes;

import java.util.List;

import classes.Evento;
import classes.Voluntario;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface EventoService {

    @GET("eventos/")
    Call<List<Evento>> buscarEventos();

    @GET("eventos/voluntarioevento/{cpf}")
    Call<List<Evento>> buscarTodosEventosVoluntario(@Path("cpf") String cpf);

    @GET("eventos/todosong/{cnpj}")
    Call<List<Evento>> buscarTodosEventosOng(@Path("cnpj") String cnpj);

    @GET("eventos/voluntarioevento/eventosquenaopossuiouser/{cpf}")
    Call<List<Evento>> buscarTodosEventosEmQueNaoExisteOVoluntario(@Path("cpf") String cpf);

    @FormUrlEncoded
    @HTTP(method = "DELETE", path="eventos/voluntarioevento/deletar", hasBody = true)
    Call<Object> removerUsuarioDoEvento(@Field("idevento") int idevento, @Field("cpf") String cpf);

    @FormUrlEncoded
    @Headers("content-type:application/x-www-form-urlencoded")
    @POST("eventos/voluntarioevento/inserir")
    Call<Object> inserirVoluntarioEvento(@Field("idevento") String idevento, @Field("cpf") String cpf);

    @FormUrlEncoded
    @Headers("content-type:application/x-www-form-urlencoded")
    @PUT("eventos/atualizar")
    Call<Object> alterarEvento(@Field("id") String idevento, @Field("cnpjong") String cnpj, @Field("descricao") String descricao, @Field("telcontato") String telcontato,
                               @Field("organizador") String organizador, @Field("localizacao") String localizacao, @Field("situacao") boolean situacao,
                               @Field("dataevento") String dataevento);

    @FormUrlEncoded
    @Headers("content-type:application/x-www-form-urlencoded")
    @POST("eventos/inserir")
    Call<Object> inserirEvento(@Field("cnpjong") String cnpj, @Field("descricao") String descricao, @Field("telcontato") String telcontato,
                               @Field("organizador") String organizador, @Field("localizacao") String localizacao, @Field("situacao") boolean situacao,
                               @Field("dataevento") String dataevento);


    @DELETE("eventos/deletar/{id}")
    Call<Object> deletarEvento(@Path("id") String idevento);

}
