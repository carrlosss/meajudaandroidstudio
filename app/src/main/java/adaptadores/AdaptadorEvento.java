package adaptadores;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import classes.Evento;
import iftm.meajuda.R;

public class AdaptadorEvento extends BaseAdapter {


    private Context context;
    private List<Evento> al_lista;

    public AdaptadorEvento(Context context, List<Evento> al_lista) {
        this.context = context;
        this.al_lista = al_lista;
    }



    @Override
    public int getCount() {
        return al_lista.size();
    }

    @Override
    public Object getItem(int position) {
        return al_lista.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Evento evento = al_lista.get(position);
        LayoutInflater inflador = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View subTela = inflador.inflate(R.layout.item_evento, null);

        TextView tv_nomeong = subTela.findViewById(R.id.TV_NOMEONG);
        TextView tv_descricaoevento = subTela.findViewById(R.id.TV_DESCRICAOEVENTO);
        TextView tv_localizacao = subTela.findViewById(R.id.TV_LOCALIZACAO);
        TextView tv_dataevento = subTela.findViewById(R.id.TV_DATAEVENTO);
        CardView cd_card = subTela.findViewById(R.id.CD_EVENTO);

        if(evento.isSituacao() == true)
            cd_card.setCardBackgroundColor(Color.rgb(19, 197, 216));
        else
            cd_card.setCardBackgroundColor(Color.rgb(196, 1, 5));

        tv_nomeong.setText("Ong: " + evento.getOng().getNomeong());
        tv_descricaoevento.setText("Descrição: " + evento.getDescricao());
        tv_localizacao.setText("Endereço: " + evento.getLocalizacao());
        tv_dataevento.setText("Data: " + evento.getDataevento());


        return(subTela);
    }
}
