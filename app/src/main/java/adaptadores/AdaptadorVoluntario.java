package adaptadores;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import classes.Evento;
import classes.Voluntario;
import iftm.meajuda.R;

public class AdaptadorVoluntario extends BaseAdapter {


    private Context context;
    private List<Voluntario> al_lista;

    public AdaptadorVoluntario(Context context, List<Voluntario> al_lista) {
        this.context = context;
        this.al_lista = al_lista;
    }



    @Override
    public int getCount() {
        return al_lista.size();
    }

    @Override
    public Object getItem(int position) {
        return al_lista.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Voluntario voluntario = al_lista.get(position);
        LayoutInflater inflador = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View subTela = inflador.inflate(R.layout.item_voluntario, null);

        TextView tv_nome = subTela.findViewById(R.id.TV_NOMEVOLUNTARIO);
        TextView tv_datanascimento = subTela.findViewById(R.id.TV_DATANASCIMENTOVOLUNTARIO);
        TextView tv_telefone = subTela.findViewById(R.id.TV_TELEFONEVOLUNTARIO);
        TextView tv_email = subTela.findViewById(R.id.TV_EMAILVOLUNTARIO);
        CardView cd_card = subTela.findViewById(R.id.CD_VOLUNTARIO);

        cd_card.setCardBackgroundColor(Color.rgb(0, 0, 0));

        tv_nome.setText("Nome: " + voluntario.getNome());
        tv_datanascimento.setText("Data nascimento: " + voluntario.getDatanascimento());
        tv_telefone.setText("Telefone: " + voluntario.getTelefone());
        tv_email.setText("Email: " + voluntario.getEmail());


        return(subTela);
    }
}
