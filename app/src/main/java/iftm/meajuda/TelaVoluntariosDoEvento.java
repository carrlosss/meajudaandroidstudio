package iftm.meajuda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.List;

import adaptadores.AdaptadorEvento;
import adaptadores.AdaptadorVoluntario;
import classes.Evento;
import classes.Session;
import classes.Voluntario;
import requisicoes.RetrofitConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TelaVoluntariosDoEvento extends AppCompatActivity {

    ListView lv_voluntarios;
    List<Voluntario> al_voluntarios;
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_voluntarios_do_evento);

        session = new Session(this);
        lv_voluntarios = findViewById(R.id.LV_VOLUNTARIOSDOEVENTO);

    }

    @Override
    protected void onStart() {
        super.onStart();
        montarlistview();
    }

    private void montarlistview(){

        Call<List<Voluntario>> call = new RetrofitConfig().getVoluntarioService().buscarTodosVoluntariosEvento(session.getideventoselecionado());

        call.enqueue(new Callback<List<Voluntario>>() {
            @Override
            public void onResponse(Call<List<Voluntario>> call, Response<List<Voluntario>> response) {

                if(response.isSuccessful()) {
                    al_voluntarios = response.body();
                    setaradapter();
                }
                else {
                    al_voluntarios = null;
                    setaradapter();
                }

            }
            @Override
            public void onFailure(Call<List<Voluntario>> call, Throwable t) {
                al_voluntarios = null;
                setaradapter();
            }
        });
    }

    private void setaradapter(){

        if(al_voluntarios == null || al_voluntarios.isEmpty())
        {
            lv_voluntarios.setAdapter(null);
        }
        else
        {
            lv_voluntarios.setAdapter(new AdaptadorVoluntario(this, al_voluntarios));
        }
    }

}
