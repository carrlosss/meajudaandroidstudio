package iftm.meajuda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import classes.ONG;
import classes.Voluntario;
import requisicoes.RetrofitConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CadastrarVoluntario extends AppCompatActivity implements View.OnClickListener {

    EditText et_cpf, et_senha1, et_senha2, et_nome, et_datanascimento, et_profissao, et_telefone, et_email, et_cidade, et_estado, et_endereco;
    Button bt_salvar, bt_limpar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastrar_voluntario);



        et_cpf = findViewById(R.id.ET_CPF);
        et_senha1 = findViewById(R.id.ET_SENHA1VOLUNTARIO);
        et_senha2 = findViewById(R.id.ET_SENHA2VOLUNTARIO);
        et_nome = findViewById(R.id.ET_NOMEVOLUNTARIO);
        et_datanascimento = findViewById(R.id.ET_DATANASCIMENTO);
        et_profissao = findViewById(R.id.ET_PROFISSAO);
        et_telefone = findViewById(R.id.ET_TELEFONE);
        et_email = findViewById(R.id.ET_EMAIL);
        et_cidade = findViewById(R.id.ET_CIDADE);
        et_estado = findViewById(R.id.ET_ESTADOVOLUNTARIO);
        et_endereco = findViewById(R.id.ET_ENDERECO);

        bt_salvar = findViewById(R.id.BT_INSERIRVOLUNTARIO);
        bt_limpar = findViewById(R.id.BT_LIMPARVOLUNTARIO);

        bt_limpar.setOnClickListener(this);
        bt_salvar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == bt_limpar.getId())
        {
            limpar();
        }
        if(v.getId() == bt_salvar.getId())
        {
            Voluntario voluntario = new Voluntario();
            voluntario.setCpf(et_cpf.getText().toString());
            voluntario.setSenha(et_senha1.getText().toString());
            voluntario.setNome(et_nome.getText().toString());
            voluntario.setDatanascimento(et_datanascimento.getText().toString());
            voluntario.setProfissao(et_profissao.getText().toString());
            voluntario.setTelefone(et_telefone.getText().toString());
            voluntario.setEmail(et_email.getText().toString());
            voluntario.setCidade(et_cidade.getText().toString());
            voluntario.setEstado(et_estado.getText().toString());
            voluntario.setEndereco(et_endereco.getText().toString());

            inserirvoluntario(voluntario);
        }
    }

    private void mensagem(String msg)
    {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }


    public void inserirvoluntario(Voluntario voluntario) {

        Call<Object> call = new RetrofitConfig().getVoluntarioService().inserirVoluntario(voluntario.getCpf(),voluntario.getSenha(),voluntario.getNome(),voluntario.getDatanascimento(),voluntario.getProfissao(),voluntario.getTelefone(),voluntario.getEmail(),voluntario.getCidade(),voluntario.getEstado(),voluntario.getEndereco());

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if(response.isSuccessful()) {
                    mensagem("Voce se cadastrou com sucesso!!");
                    limpar();
                }
                else {
                    mensagem("Usuario ja existente!!");
                }

            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mensagem("Erro no servidor!!");
            }
        });
    }


    public void limpar() {

        et_cpf.setText("");
        et_senha1.setText("");
        et_senha2.setText("");
        et_nome.setText("");
        et_datanascimento.setText("");
        et_profissao.setText("");
        et_telefone.setText("");
        et_email.setText("");
        et_cidade.setText("");
        et_estado.setText("");
        et_endereco.setText("");

    }
}
