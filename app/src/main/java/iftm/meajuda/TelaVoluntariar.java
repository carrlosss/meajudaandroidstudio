package iftm.meajuda;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import adaptadores.AdaptadorEvento;
import classes.Evento;
import classes.Session;
import requisicoes.RetrofitConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TelaVoluntariar extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ListView lv_eventos;
    private List<Evento> al_eventos;
    private Session session;
    private Evento eventoselecionado;
    private AlertDialog alerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_voluntariar);


        lv_eventos = findViewById(R.id.LV_EVENTOSDISPONIVEIS);
        lv_eventos.setOnItemClickListener(this);
        session = new Session(this);

        montarlistview();

    }
    private void mensagem(String msg)
    {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        montarlistview();
    }

    private void montarlistview(){

        Call<List<Evento>> call = new RetrofitConfig().getEventoService().buscarTodosEventosEmQueNaoExisteOVoluntario(session.getidusuario());

        call.enqueue(new Callback<List<Evento>>() {
            @Override
            public void onResponse(Call<List<Evento>> call, Response<List<Evento>> response) {

                if(response.isSuccessful()) {
                    al_eventos = response.body();
                    setaradapter();
                }
                else {
                    al_eventos = null;
                    setaradapter();
                }

            }
            @Override
            public void onFailure(Call<List<Evento>> call, Throwable t) {
                al_eventos = null;
                setaradapter();
            }
        });
    }

    private void setaradapter(){

        if(al_eventos == null || al_eventos.isEmpty())
        {
            lv_eventos.setAdapter(null);
        }
        else
        {
            lv_eventos.setAdapter(new AdaptadorEvento(this, al_eventos));
        }
    }


    public void comecarinsercao() {
        AlertDialog.Builder construtor = new AlertDialog.Builder(this);
        construtor.setTitle("Voluntariar-se");
        construtor.setMessage("Você deseja se voluntariar neste evento?");

        construtor.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                inserirvoluntarionoevento();
            }
        });
        construtor.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        alerta = construtor.create();
        alerta.show();
    }

    public void inserirvoluntarionoevento() {

        Call<Object> call = new RetrofitConfig().getEventoService().inserirVoluntarioEvento(String.valueOf(eventoselecionado.getIdevento()), session.getidusuario());

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if(response.isSuccessful()) {
                    mensagem("Voce se voluntariou com sucesso!!");
                    montarlistview();
                }
                else {
                    mensagem("Erro ao se voluntariar!!");
                }

            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.i("TESTE", t.getMessage());
                mensagem("Erro no servidor!!");
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        eventoselecionado = al_eventos.get(position);
        comecarinsercao();
    }
}
