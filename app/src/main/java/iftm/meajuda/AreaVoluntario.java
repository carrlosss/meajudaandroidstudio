package iftm.meajuda;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adaptadores.AdaptadorEvento;
import classes.Evento;
import classes.ONG;
import classes.Session;
import requisicoes.RetrofitConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AreaVoluntario extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private ListView lv_eventos;
    private List<Evento> al_eventos;
    private Session session;
    private Button bt_voluntariar, bt_legendatrue, bt_legendafalse, bt_perfil;
    private Intent it_carregaTela;
    private AlertDialog alerta;
    private Evento eventoselecionado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.area_voluntario);

        lv_eventos = findViewById(R.id.LV_EVENTOS);
        session = new Session(this);
        bt_voluntariar = findViewById(R.id.BT_VOLUNTARIAR);
        bt_voluntariar.setOnClickListener(this);
        bt_perfil = findViewById(R.id.BT_PERFILVOLUNTARIO);
        bt_perfil.setOnClickListener(this);
        bt_legendatrue = findViewById(R.id.BT_LEGENDATRUE);
        bt_legendafalse = findViewById(R.id.BT_LEGENDAFALSE);

        bt_legendatrue.setBackgroundColor(Color.rgb(19, 197, 216));
        bt_legendafalse.setBackgroundColor(Color.rgb(196, 1, 5));
        bt_legendafalse.setClickable(false);
        bt_legendatrue.setClickable(false);

        lv_eventos.setOnItemClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        montarlistview();
    }

    @Override
    public void onBackPressed() {
        session.logout();
        if (true) {
            finish();
        }
    }

    private void montarlistview(){

       Call<List<Evento>> call = new RetrofitConfig().getEventoService().buscarTodosEventosVoluntario(session.getidusuario());

        call.enqueue(new Callback<List<Evento>>() {
            @Override
            public void onResponse(Call<List<Evento>> call, Response<List<Evento>> response) {

                if(response.isSuccessful()) {
                    al_eventos = response.body();
                    setaradapter();
                }
                else {
                    al_eventos = null;
                    setaradapter();
                }

            }
            @Override
            public void onFailure(Call<List<Evento>> call, Throwable t) {
                al_eventos = null;
                setaradapter();
            }
        });
    }

    private void setaradapter(){

        if(al_eventos == null || al_eventos.isEmpty())
        {
            lv_eventos.setAdapter(null);
        }
        else
        {
            lv_eventos.setAdapter(new AdaptadorEvento(this, al_eventos));
        }
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == bt_voluntariar.getId())
        {
            it_carregaTela = new Intent(AreaVoluntario.this, TelaVoluntariar.class);
            this.startActivity(it_carregaTela);
        }
        else if(v.getId() == bt_perfil.getId())
        {
            it_carregaTela = new Intent(AreaVoluntario.this, PerfilVoluntario.class);
            this.startActivity(it_carregaTela);
        }
    }

    public void comecarinsercao() {
        AlertDialog.Builder construtor = new AlertDialog.Builder(this);
        construtor.setTitle("Remover intenção de voluntário.");
        construtor.setMessage("Você deseja remover sua intenção de voluntariar-se deste evento?");

        construtor.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                deletarvoluntarionoevento();
            }
        });
        construtor.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        alerta = construtor.create();
        alerta.show();
    }

    private void mensagem(String msg)
    {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }


    public void deletarvoluntarionoevento() {

        Call<Object> call = new RetrofitConfig().getEventoService().removerUsuarioDoEvento(eventoselecionado.getIdevento(), session.getidusuario());

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if(response.isSuccessful()) {
                    mensagem("Cancelado com sucesso!!");
                    montarlistview();
                }
                else {
                    mensagem("Erro ao cancelar!!");
                }

            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mensagem("Erro no servidor!!");
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        eventoselecionado = al_eventos.get(position);
        comecarinsercao();

    }


}
