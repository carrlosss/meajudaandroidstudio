package iftm.meajuda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import java.util.List;

import adaptadores.AdaptadorEvento;
import classes.Evento;
import classes.ONG;
import classes.Session;
import requisicoes.RetrofitConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventoTela extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private Evento eventoselecionado;
    private List<Evento> al_eventos;
    private EditText et_descricao, et_telContato, et_localizacao, et_dataevento, et_organizador;
    private Button bt_alterar, bt_inserir, bt_deletar, bt_confirmar, bt_cancelar, bt_finalizar;
    private Session session;
    private ListView lv_eventos;
    private int estado = 0;
    private Evento evento;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.evento);

        et_descricao = findViewById(R.id.ET_DESCRICAOEVENTO);
        et_telContato = findViewById(R.id.ET_TELEFONEEVENTO);
        et_localizacao = findViewById(R.id.ET_LOCALIZACAOEVENTO);
        et_dataevento = findViewById(R.id.ET_DATAEVENTO);
        et_organizador = findViewById(R.id.ET_ORGANIZADOREVENTO);

        bt_alterar  = findViewById(R.id.BT_ALTERAREVENTO);
        bt_inserir = findViewById(R.id.BT_INSERIREVENTO);
        bt_deletar = findViewById(R.id.BT_APAGAREVENTO);
        bt_cancelar = findViewById(R.id.BT_CANCELAREVENTO);
        bt_confirmar = findViewById(R.id.BT_CONFIRMAREVENTO);
        bt_finalizar = findViewById(R.id.BT_FINALIZAREVENTO);

        bt_finalizar.setOnClickListener(this);
        bt_confirmar.setOnClickListener(this);
        bt_cancelar.setOnClickListener(this);
        bt_alterar.setOnClickListener(this);
        bt_inserir.setOnClickListener(this);
        bt_deletar.setOnClickListener(this);

        lv_eventos = findViewById(R.id.LV_EVENTOSONGCRUD);
        lv_eventos.setOnItemClickListener(this);
        session = new Session(this);

    }
     private Evento getInputs(){

         Evento e = null;

         if(validacampos()){

            e = new Evento();
            ONG ong = new ONG();
            ong.setCnpj(session.getidusuario());

            e.setOng(ong);
            e.setDescricao(et_descricao.getText().toString());
            e.setTelcontato(et_telContato.getText().toString());
            e.setLocalizacao(et_localizacao.getText().toString());
            e.setSituacao(false);
            e.setDataevento(et_dataevento.getText().toString());
            e.setOrganizador(et_organizador.getText().toString());
        }
        return e;
     }

    private Boolean validacampos(){
       return true;
    }
    @Override
    public void onClick(View v) {

        if(v.getId() == bt_alterar.getId())
        {
            estado = 2;
            habilitarcampos(true);

        }
        else if(v.getId() == bt_inserir.getId())
        {
            limpar();
            estado = 1;
            habilitarcampos(true);
        }
        else if(v.getId() == bt_deletar.getId())
        {
            if(eventoselecionado != null)
                deletarevevento();
        }
        else if(v.getId() == bt_cancelar.getId())
        {
            habilitarcampos(false);
            limpar();
        }
        else if(v.getId() == bt_confirmar.getId())
        {
            habilitarcampos(false);
            Evento e = getInputs();
            if(estado == 1)
                inserirevevento(e);
            else if(estado == 2)
            {
                e.setIdevento(eventoselecionado.getIdevento());
                alterarevento(e);
            }

        }
        else if(bt_finalizar.getId() == v.getId())
        {
            Evento e = eventoselecionado;
            e.setSituacao(true);
            finalizarevento(e);
        }
    }

    private void mensagem(String msg)
    {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }


    public void inserirevevento(Evento evento) {

        Call<Object> call = new RetrofitConfig().getEventoService().inserirEvento(evento.getOng().getCnpj(), evento.getDescricao(),evento.getTelcontato(),evento.getOrganizador(),evento.getLocalizacao(),evento.isSituacao(),evento.getDataevento());

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if(response.isSuccessful()) {
                    mensagem("Evento criado com sucesso!!");
                    montarlistview();
                    limpar();
                }
                else {
                    mensagem("Erro ao criar evento!!");
                }

            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mensagem("Erro no servidor!!");
            }
        });
    }
    public void deletarevevento() {

        Call<Object> call = new RetrofitConfig().getEventoService().deletarEvento(String.valueOf(eventoselecionado.getIdevento()));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if(response.isSuccessful()) {
                    mensagem("Evento deletado com sucesso!!");
                    montarlistview();
                    eventoselecionado = null;
                    limpar();
                }
                else {
                    mensagem("Erro ao deletar evento!!");
                }

            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mensagem("Erro no servidor!!");
            }
        });
    }

    public void alterarevento(final Evento evento) {

        Call<Object> call = new RetrofitConfig().getEventoService().alterarEvento(String.valueOf(evento.getIdevento()),evento.getOng().getCnpj(), evento.getDescricao(),evento.getTelcontato(),evento.getOrganizador(),evento.getLocalizacao(),evento.isSituacao(),evento.getDataevento());

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if(response.isSuccessful()) {
                    mensagem("Evento alterado com sucesso!!");
                    eventoselecionado = evento;
                    setarcampos(eventoselecionado);
                    montarlistview();
                }
                else {
                    mensagem("Erro ao alterar evento!!");
                }

            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mensagem("Erro no servidor!!");
                Log.i("TESTE",t.getMessage());
            }
        });
    }

    public void finalizarevento(final Evento evento) {

        Call<Object> call = new RetrofitConfig().getEventoService().alterarEvento(String.valueOf(evento.getIdevento()),evento.getOng().getCnpj(), evento.getDescricao(),evento.getTelcontato(),evento.getOrganizador(),evento.getLocalizacao(),evento.isSituacao(),evento.getDataevento());

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if(response.isSuccessful()) {
                    mensagem("Evento finalizado com sucesso!!");
                    eventoselecionado = evento;
                    setarcampos(eventoselecionado);
                    montarlistview();
                }
                else {
                    mensagem("Erro ao finalizar evento!!");
                }

            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mensagem("Erro no servidor!!");
                Log.i("TESTE",t.getMessage());
            }
        });
    }


    public void limpar() {

        et_descricao.setText("");
        et_dataevento.setText("");
        et_localizacao.setText("");
        et_organizador.setText("");
        et_telContato.setText("");

    }

    @Override
    protected void onStart() {
        super.onStart();
        montarlistview();
        habilitarcampos(false);
    }

    private void montarlistview(){

        Call<List<Evento>> call = new RetrofitConfig().getEventoService().buscarTodosEventosOng(session.getidusuario());

        call.enqueue(new Callback<List<Evento>>() {
            @Override
            public void onResponse(Call<List<Evento>> call, Response<List<Evento>> response) {

                if(response.isSuccessful()) {
                    al_eventos = response.body();
                    setaradapter();
                }
                else {
                    al_eventos = null;
                    setaradapter();
                }

            }
            @Override
            public void onFailure(Call<List<Evento>> call, Throwable t) {
                al_eventos = null;
                setaradapter();
            }
        });
    }

    private void setaradapter(){

        if(al_eventos == null || al_eventos.isEmpty())
        {
            lv_eventos.setAdapter(null);
        }
        else
        {
            lv_eventos.setAdapter(new AdaptadorEvento(this, al_eventos));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        eventoselecionado = al_eventos.get(position);
        Log.i("EVENTOSELECIONADO",eventoselecionado.toString());
        setarcampos(eventoselecionado);
        habilitarcampos(false);
    }

    public void setarcampos(Evento e) {

        et_dataevento.setText(e.getDataevento());
        et_localizacao.setText(e.getLocalizacao());
        et_organizador.setText(e.getOrganizador());
        et_descricao.setText(e.getDescricao());
        et_telContato.setText(e.getTelcontato());
    }

    public void habilitarcampos(boolean estaeditando) {

        et_dataevento.setEnabled(estaeditando);
        et_localizacao.setEnabled(estaeditando);
        et_organizador.setEnabled(estaeditando);
        et_descricao.setEnabled(estaeditando);
        et_telContato.setEnabled(estaeditando);


        bt_deletar.setEnabled(!estaeditando);
        bt_inserir.setEnabled(!estaeditando);
        bt_alterar.setEnabled(!estaeditando);
        bt_confirmar.setEnabled(estaeditando);
        bt_cancelar.setEnabled(estaeditando);
    }
}

