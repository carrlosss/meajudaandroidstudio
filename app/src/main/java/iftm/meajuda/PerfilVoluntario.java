package iftm.meajuda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import classes.Session;
import classes.Voluntario;
import requisicoes.RetrofitConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PerfilVoluntario extends AppCompatActivity implements View.OnClickListener {

    EditText et_cpf, et_senha1, et_nome, et_datanascimento, et_profissao, et_telefone, et_email, et_cidade, et_estado, et_endereco;

    Button bt_alterar, bt_cancelar, bt_confirmar;
    Session session;
    Voluntario voluntariocerto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfil_voluntario);


        et_cpf = findViewById(R.id.ET_CPFPERFIL);
        et_senha1 = findViewById(R.id.ET_SENHAVOLUNTARIOPERFIL);
        et_nome = findViewById(R.id.ET_NOMEVOLUNTARIOPERFIL);
        et_datanascimento = findViewById(R.id.ET_DATANASCIMENTOPERFIL);
        et_profissao = findViewById(R.id.ET_PROFISSAOPERFIL);
        et_telefone = findViewById(R.id.ET_TELEFONEPERFIL);
        et_email = findViewById(R.id.ET_EMAILPERFIL);
        et_cidade = findViewById(R.id.ET_CIDADEPERFIL);
        et_estado = findViewById(R.id.ET_ESTADOVOLUNTARIOPERFIL);
        et_endereco = findViewById(R.id.ET_ENDERECOPERFIL);

        bt_alterar = findViewById(R.id.BT_ALTERARVOLUNTARIO);
        bt_cancelar = findViewById(R.id.BT_CANCELAVOLUNTARIO);
        bt_confirmar = findViewById(R.id.BT_CONFIRMAVOLUNTARIO);

        bt_cancelar.setClickable(false);
        bt_confirmar.setClickable(false);

        bt_confirmar.setOnClickListener(this);
        bt_cancelar.setOnClickListener(this);
        bt_alterar.setOnClickListener(this);

        et_cpf.setEnabled(false);
        session = new Session(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == bt_alterar.getId())
        {
            habilitarcampos(true);
            et_nome.requestFocus();
        }
        else if(v.getId() == bt_cancelar.getId())
        {
            habilitarcampos(false);
            setarinfosvoluntario(voluntariocerto);
        }
        else if(v.getId() == bt_confirmar.getId())
        {
            Voluntario voluntario = new Voluntario();
            voluntario.setCpf(et_cpf.getText().toString());
            voluntario.setSenha(et_senha1.getText().toString());
            voluntario.setNome(et_nome.getText().toString());
            voluntario.setDatanascimento(et_datanascimento.getText().toString());
            voluntario.setProfissao(et_profissao.getText().toString());
            voluntario.setTelefone(et_telefone.getText().toString());
            voluntario.setEmail(et_email.getText().toString());
            voluntario.setCidade(et_cidade.getText().toString());
            voluntario.setEstado(et_estado.getText().toString());
            voluntario.setEndereco(et_endereco.getText().toString());

            atualizarvoluntario(voluntario);
        }

    }


    public void atualizarvoluntario(final Voluntario voluntario) {

        Call<Object> call = new RetrofitConfig().getVoluntarioService().alterarVoluntario(voluntario.getCpf(),voluntario.getSenha(),voluntario.getNome(),voluntario.getDatanascimento(),voluntario.getProfissao(),voluntario.getTelefone(),voluntario.getEmail(),voluntario.getCidade(),voluntario.getEstado(),voluntario.getEndereco());

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if(response.isSuccessful()) {
                    session.setnomeusuario(voluntario.getNome());
                    voluntariocerto = voluntario;
                    setarinfosvoluntario(voluntariocerto);
                    mensagem("Informações atualizadas com sucesso!!");
                    habilitarcampos(false);
                }
                else {
                    mensagem("Erro ao atualizar!!");
                }

            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mensagem("Erro no servidor!!");
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        habilitarcampos(false);
        buscarinfosvoluntario();
    }


    private void mensagem(String msg)
    {
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
    }

    public void buscarinfosvoluntario() {
        Call<Voluntario> call = new RetrofitConfig().getVoluntarioService().buscarVoluntario(session.getidusuario());

        call.enqueue(new Callback<Voluntario>() {
            @Override
            public void onResponse(Call<Voluntario> call, Response<Voluntario> response) {

                if(response.isSuccessful()) {
                    voluntariocerto = response.body();
                    setarinfosvoluntario(voluntariocerto);
                }
            }

            @Override
            public void onFailure(Call<Voluntario> call, Throwable t) {

                mensagem("Erro Erroso");
            }
        });
    }

    public void setarinfosvoluntario(Voluntario voluntario) {

        et_cpf.setText(voluntario.getCpf());
        et_senha1.setText(voluntario.getSenha());
        et_nome.setText(voluntario.getNome());
        et_datanascimento.setText(voluntario.getDatanascimento());
        if(voluntario.getProfissao() != null)
            et_profissao.setText(voluntario.getProfissao());
        else
            et_profissao.setText("");
        et_telefone.setText(voluntario.getTelefone());
        et_email.setText(voluntario.getEmail());
        et_cidade.setText(voluntario.getCidade());
        et_estado.setText(voluntario.getEstado());
        et_endereco.setText(voluntario.getEndereco());

    }

    public void habilitarcampos(boolean estaeditando) {

        et_senha1.setEnabled(estaeditando);
        et_nome.setEnabled(estaeditando);
        et_datanascimento.setEnabled(estaeditando);
        et_profissao.setEnabled(estaeditando);
        et_telefone.setEnabled(estaeditando);
        et_email.setEnabled(estaeditando);
        et_cidade.setEnabled(estaeditando);
        et_estado.setEnabled(estaeditando);
        et_endereco.setEnabled(estaeditando);

        bt_alterar.setEnabled(!estaeditando);
        bt_confirmar.setEnabled(estaeditando);
        bt_cancelar.setEnabled(estaeditando);
    }
}
