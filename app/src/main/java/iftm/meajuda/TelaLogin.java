package iftm.meajuda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import classes.ONG;
import classes.Session;
import classes.Voluntario;
import requisicoes.RetrofitConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TelaLogin extends AppCompatActivity implements View.OnClickListener {

    private Intent it_carregatela;
    private Button bt_logar, bt_cadastrarong, bt_cadastrarvoluntario;
    private EditText et_login, et_senha;
    private RadioGroup rg_tipousuarios;
    private RadioButton rb_ong, rb_voluntario;
    private Session session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_login);

        bt_logar = findViewById(R.id.BT_LOGIN);
        bt_cadastrarong = findViewById(R.id.BT_CADASTROONG);
        bt_cadastrarvoluntario = findViewById(R.id.BT_CADASTROVOLUNTARIO);

        et_login = findViewById(R.id.ET_LOGIN);
        et_senha = findViewById(R.id.ET_SENHA);

        bt_logar.setOnClickListener(this);
        bt_cadastrarong.setOnClickListener(this);
        bt_cadastrarvoluntario.setOnClickListener(this);
        rg_tipousuarios = findViewById(R.id.RG_TIPOUSER);
        rb_ong = findViewById(R.id.RB_ONG);
        rb_voluntario = findViewById(R.id.RB_VOLUNTARIO);

        session = new Session(this);

        alterartela();
    }

    private void mensagem(String msg)
    {
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
    }



    @Override
    public void onClick(View v) {
        if(v.getId() == bt_logar.getId())
        {
            if(rg_tipousuarios.getCheckedRadioButtonId() == rb_ong.getId())
            {
                logarong();
            }
            else if(rg_tipousuarios.getCheckedRadioButtonId() == rb_voluntario.getId())
            {
                logarvoluntario();
            }
        }
        if(v.getId() == bt_cadastrarong.getId())
        {
            this.it_carregatela = new Intent(TelaLogin.this, CadastrarOng.class);
            this.startActivity(it_carregatela);
        }
        if(v.getId() == bt_cadastrarvoluntario.getId())
        {
            this.it_carregatela = new Intent(TelaLogin.this, CadastrarVoluntario.class);
            this.startActivity(it_carregatela);
        }
    }

    public void logarong() {
        Call<ONG> call = new RetrofitConfig().getOngService().buscarOng(et_login.getText().toString());

        call.enqueue(new Callback<ONG>() {
            @Override
            public void onResponse(Call<ONG> call, Response<ONG> response) {

                if(response.isSuccessful()) {
                    ONG ong = response.body();
                    if (ong.getSenha().equals(et_senha.getText().toString())) {
                        session.setlogado(true);
                        session.setidusuario(ong.getCnpj());
                        session.setnomeusuario(ong.getNomeong());
                        session.settipousuario("Ong");
                        alterartela();
                    }
                    else
                    {
                        mensagem("Senha incorreta.");
                    }
                }
                else
                    mensagem("Usuario não existe.");
            }

            @Override
            public void onFailure(Call<ONG> call, Throwable t) {

                mensagem("Erro Erroso");
            }
        });
    }

    public void logarvoluntario() {
        Call<Voluntario> call = new RetrofitConfig().getVoluntarioService().buscarVoluntario(et_login.getText().toString());

        call.enqueue(new Callback<Voluntario>() {
            @Override
            public void onResponse(Call<Voluntario> call, Response<Voluntario> response) {

                if(response.isSuccessful()) {
                    Voluntario voluntario = response.body();
                    if (voluntario.getSenha().equals(et_senha.getText().toString())) {
                        session.setlogado(true);
                        session.setidusuario(voluntario.getCpf());
                        session.setnomeusuario(voluntario.getNome());
                        session.settipousuario("Voluntario");
                        alterartela();
                    }
                    else
                    {
                        mensagem("Senha incorreta.");

                    }
                }
                else
                    mensagem("Usuario não existe.");
            }

            @Override
            public void onFailure(Call<Voluntario> call, Throwable t) {

                mensagem("Erro Erroso");
            }
        });
    }


    public void alterartela() {
        if(session.getlogado())
        {
            if(session.gettipousuario().equals("Ong"))
            {
                this.it_carregatela = new Intent(TelaLogin.this, AreaOng.class);
                this.startActivity(it_carregatela);
            }
            else if(session.gettipousuario().equals("Voluntario"))
            {
                this.it_carregatela = new Intent(TelaLogin.this, AreaVoluntario.class);
                this.startActivity(it_carregatela);
            }
        }
    }
}
