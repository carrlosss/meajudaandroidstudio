package iftm.meajuda;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import adaptadores.AdaptadorEvento;
import classes.Evento;
import classes.Session;
import requisicoes.RetrofitConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AreaOng extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private Session session;
    private Button bt_legendatrue, bt_legendafalse, bt_perfil, bt_eventos;
    private ListView lv_eventos;
    private List<Evento> al_eventos;
    private Intent it_carregaTela;
    private AlertDialog alerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.area_ong);

        session = new Session(this);

        bt_perfil = findViewById(R.id.BT_PERFILONG);
        bt_perfil.setOnClickListener(this);

        bt_legendatrue = findViewById(R.id.BT_LEGENDATRUEONG);
        bt_legendafalse = findViewById(R.id.BT_LEGENDAFALSEONG);
        bt_eventos = findViewById(R.id.BT_CADASTRAREVENTOS);
        bt_eventos.setOnClickListener(this);

        bt_legendatrue.setBackgroundColor(Color.rgb(19, 197, 216));
        bt_legendafalse.setBackgroundColor(Color.rgb(196, 1, 5));
        bt_legendafalse.setClickable(false);
        bt_legendatrue.setClickable(false);

        lv_eventos = findViewById(R.id.LV_EVENTOSONG);
        lv_eventos.setOnItemClickListener(this);

    }

    @Override
    public void onBackPressed() {
        session.logout();
        if (true) {
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        montarlistview();
    }

    private void montarlistview(){

        Call<List<Evento>> call = new RetrofitConfig().getEventoService().buscarTodosEventosOng(session.getidusuario());

        call.enqueue(new Callback<List<Evento>>() {
            @Override
            public void onResponse(Call<List<Evento>> call, Response<List<Evento>> response) {

                if(response.isSuccessful()) {
                    al_eventos = response.body();
                    setaradapter();
                }
                else {
                    al_eventos = null;
                    setaradapter();
                }

            }
            @Override
            public void onFailure(Call<List<Evento>> call, Throwable t) {
                al_eventos = null;
                setaradapter();
            }
        });
    }

    private void setaradapter(){

        if(al_eventos == null || al_eventos.isEmpty())
        {
            lv_eventos.setAdapter(null);
        }
        else
        {
            lv_eventos.setAdapter(new AdaptadorEvento(this, al_eventos));
        }
    }


    @Override
    public void onClick(View v) {

        if(v.getId() == bt_perfil.getId())
        {
//            it_carregaTela = new Intent(AreaOng.this, PerfilOng.class);
//            this.startActivity(it_carregaTela);
        }
        else if(v.getId() == bt_eventos.getId())
        {
            it_carregaTela = new Intent(AreaOng.this, EventoTela.class);
            this.startActivity(it_carregaTela);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        session.setideventoselecionado(al_eventos.get(position).getIdevento());
        alertarsobreteladevoluntarios();
    }

    public void alertarsobreteladevoluntarios() {
        AlertDialog.Builder construtor = new AlertDialog.Builder(this);
        construtor.setTitle("Visualizar Voluntarios");
        construtor.setMessage("Você deseja visualizar todos os voluntarios deste evento?");

        construtor.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                abrirteladevoluntarios();
            }
        });
        construtor.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        alerta = construtor.create();
        alerta.show();
    }

    public void abrirteladevoluntarios() {

        it_carregaTela = new Intent(AreaOng.this, TelaVoluntariosDoEvento.class);
        this.startActivity(it_carregaTela);
    }
}
