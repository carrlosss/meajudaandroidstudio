package iftm.meajuda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import classes.ONG;
import requisicoes.RetrofitConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CadastrarOng extends AppCompatActivity implements View.OnClickListener {

    EditText et_cnpj, et_senha1, et_senha2, et_nomeong,et_nomeresponsavel, et_razaosocial, et_datafundacao,
            et_cidadeong, et_estadoong, et_enderecoong, et_telefoneong, et_emailong;
    Button bt_salvar, bt_limpar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastrarong);


        et_cnpj = findViewById(R.id.ET_CNPJ);
        et_senha1 = findViewById(R.id.ET_SENHA1ONG);
        et_senha2 = findViewById(R.id.ET_SENHA2ONG);
        et_nomeong = findViewById(R.id.ET_NOMEONG);
        et_nomeresponsavel = findViewById(R.id.ET_RESPONSAVEL);
        et_razaosocial = findViewById(R.id.ET_RAZAOSOCIAL);
        et_datafundacao = findViewById(R.id.ET_DATAFUNDACAO);
        et_cidadeong = findViewById(R.id.ET_CIDADEONG);
        et_estadoong = findViewById(R.id.ET_ESTADOONG);
        et_enderecoong = findViewById(R.id.ET_ENDERECOONG);
        et_telefoneong = findViewById(R.id.ET_TELEFONEONG);
        et_emailong = findViewById(R.id.ET_EMAILONG);

        bt_salvar = findViewById(R.id.BT_SALVARONG);
        bt_limpar = findViewById(R.id.BT_LIMPARONG);

        bt_limpar.setOnClickListener(this);
        bt_salvar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == bt_limpar.getId())
        {
            limpar();
        }
        if(v.getId() == bt_salvar.getId())
        {
            ONG ong = new ONG();
            ong.setCnpj(et_cnpj.getText().toString());
            ong.setSenha(et_senha1.getText().toString());
            ong.setNomeong(et_nomeong.getText().toString());
            ong.setNomeresponsavel(et_nomeresponsavel.getText().toString());
            ong.setRazaosocial(et_razaosocial.getText().toString());
            ong.setDatafundacao(et_datafundacao.getText().toString());
            ong.setEndereco(et_enderecoong.getText().toString());
            ong.setCidade(et_cidadeong.getText().toString());
            ong.setEstado(et_estadoong.getText().toString());
            ong.setTelefone(et_telefoneong.getText().toString());
            ong.setEmail(et_emailong.getText().toString());

            inserirong(ong);
        }
    }

    private void mensagem(String msg)
    {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }


    public void inserirong(ONG ong) {

        Call<Object> call = new RetrofitConfig().getOngService().inserirOng(ong.getCnpj(),ong.getSenha(),ong.getNomeong(),ong.getNomeresponsavel(),ong.getRazaosocial(),ong.getDatafundacao(),ong.getEndereco(),ong.getCidade(),ong.getEstado(),ong.getTelefone(),ong.getEmail());

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if(response.isSuccessful()) {
                    mensagem("Voce se cadastrou com sucesso!!");
                    limpar();
                }
                else {
                    mensagem("Usuario ja existente!!");
                }

            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mensagem("Erro no servidor!!");
            }
        });
    }


    public void limpar() {

        et_cnpj.setText("");
        et_senha1.setText("");
        et_senha2.setText("");
        et_nomeong.setText("");
        et_nomeresponsavel.setText("");
        et_razaosocial.setText("");
        et_datafundacao.setText("");
        et_cidadeong.setText("");
        et_estadoong.setText("");
        et_enderecoong.setText("");
        et_telefoneong.setText("");
        et_emailong.setText("");
    }
}
