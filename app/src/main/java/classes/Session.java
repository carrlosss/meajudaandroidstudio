package classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {
    private SharedPreferences prefs;

    public Session(Context cntx) {
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setidusuario(String id) {
        prefs.edit().putString("id", id).apply();
    }
    public String getidusuario() {
        return prefs.getString("id", "");
    }

    public void setnomeusuario(String usuario) {
        prefs.edit().putString("usuario", usuario).apply();
    }
    public String getnomeusuario() {
        String nomeusuario = prefs.getString("usuario", "");
        return nomeusuario;
    }

    public void setlogado(boolean logado) {
        prefs.edit().putBoolean("logado", logado).apply();
    }
    public boolean getlogado() {
        boolean logado = prefs.getBoolean("logado", false);
        return logado;
    }

    public void settipousuario(String tipo) {
        prefs.edit().putString("tipouser", tipo).apply();
    }
    public String gettipousuario(){
        String tipo = prefs.getString("tipouser", "");
        return tipo;
    }

    public void setideventoselecionado(int idevento) {
        prefs.edit().putInt("idevento", idevento).apply();
    }
    public int getideventoselecionado(){
        int idevento = prefs.getInt("idevento", 0);
        return idevento;
    }


    public void logout(){
        prefs.edit().clear().apply();
    }

}
