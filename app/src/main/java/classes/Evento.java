package classes;

public class Evento {

    private int idevento;
    private ONG ong;
    private String descricao;
    private String telcontato;
    private String organizador;
    private String localizacao;
    private boolean situacao;
    private String dataevento;

    public Evento() {
    }

    public Evento(ONG ong, String descricao, String telcontato, String organizador, String localizacao, boolean situacao, String dataevento) {
        this.ong = ong;
        this.descricao = descricao;
        this.telcontato = telcontato;
        this.organizador = organizador;
        this.localizacao = localizacao;
        this.situacao = situacao;
        this.dataevento = dataevento;
    }

    public Evento(int idevento, ONG ong, String descricao, String telcontato, String organizador, String localizacao, boolean situacao, String dataevento) {
        this.idevento = idevento;
        this.ong = ong;
        this.descricao = descricao;
        this.telcontato = telcontato;
        this.organizador = organizador;
        this.localizacao = localizacao;
        this.situacao = situacao;
        this.dataevento = dataevento;
    }

    public int getIdevento() {
        return idevento;
    }

    public void setIdevento(int idevento) {
        this.idevento = idevento;
    }

    public ONG getOng() {
        return ong;
    }

    public void setOng(ONG ong) {
        this.ong = ong;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTelcontato() {
        return telcontato;
    }

    public void setTelcontato(String telcontato) {
        this.telcontato = telcontato;
    }

    public String getOrganizador() {
        return organizador;
    }

    public void setOrganizador(String organizador) {
        this.organizador = organizador;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public boolean isSituacao() {
        return situacao;
    }

    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

    public String getDataevento() {
        return dataevento;
    }

    public void setDataevento(String dataevento) {
        this.dataevento = dataevento;
    }

    @Override
    public String toString() {
        return "Evento{" +
                "idevento=" + idevento +
                ", ong=" + ong +
                ", descricao='" + descricao + '\'' +
                ", telcontato='" + telcontato + '\'' +
                ", organizador='" + organizador + '\'' +
                ", localizacao='" + localizacao + '\'' +
                ", situacao=" + situacao +
                ", dataevento='" + dataevento + '\'' +
                '}';
    }
}
