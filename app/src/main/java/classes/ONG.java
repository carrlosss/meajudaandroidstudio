/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author Bancoso
 */
public class ONG {

    private String cnpj;
    private String senha;
    private String nomeong;
    private String nomeresponsavel;
    private String razaosocial;
    private String datafundacao;
    private String endereco;
    private String cidade;
    private String estado;
    private String telefone;
    private String email;


    public ONG() {

    }
    public ONG(String cnpj, String senha, String nomeong, String nomeresponsavel, String razaosocial, String datafundacao, String endereco, String cidade, String estado, String telefone, String email) {
        this.cnpj = cnpj;
        this.senha = senha;
        this.nomeong = nomeong;
        this.nomeresponsavel = nomeresponsavel;
        this.razaosocial = razaosocial;
        this.datafundacao = datafundacao;
        this.endereco = endereco;
        this.cidade = cidade;
        this.estado = estado;
        this.telefone = telefone;
        this.email = email;
    }


    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNomeong() {
        return nomeong;
    }

    public void setNomeong(String nomeong) {
        this.nomeong = nomeong;
    }

    public String getNomeresponsavel() {
        return nomeresponsavel;
    }

    public void setNomeresponsavel(String nomeresponsavel) {
        this.nomeresponsavel = nomeresponsavel;
    }

    public String getRazaosocial() {
        return razaosocial;
    }

    public void setRazaosocial(String razaosocial) {
        this.razaosocial = razaosocial;
    }

    public String getDatafundacao() {
        return datafundacao;
    }

    public void setDatafundacao(String datafundacao) {
        this.datafundacao = datafundacao;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ONG{" +
                "cnpj='" + cnpj + '\'' +
                ", senha='" + senha + '\'' +
                ", nomeong='" + nomeong + '\'' +
                ", nomeresponsavel='" + nomeresponsavel + '\'' +
                ", razaosocial='" + razaosocial + '\'' +
                ", datafundacao='" + datafundacao + '\'' +
                ", endereco='" + endereco + '\'' +
                ", cidade='" + cidade + '\'' +
                ", estado='" + estado + '\'' +
                ", telefone='" + telefone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
